@extends('frontend.layouts.app')

@section('title', app_name() . ' | Login')

@section('content')

<div class="main-content">
    <span style="height:100px; display: block"></span>
    <div class="row">
      <div class="small-6 medium-4 medium-centered columns">
        {{ Form::open(['route' => 'frontend.auth.login.post', 'class' => 'callout text-center']) }}
          <h4>{{trans('labels.frontend.auth.login_box_title')}}</h4>
          <div class="floated-label-wrapper">
            <label for="email">{{trans('validation.attributes.frontend.email')}}</label>
            {{ Form::label('email', trans('validation.attributes.frontend.email')) }}
            {{ Form::email('email', null, ['id'=>'email', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
          </div>
          <div class="floated-label-wrapper">
            {{ Form::label('pass', trans('validation.attributes.frontend.password')) }}
            {{ Form::password('password', ['id' => 'pass', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
          </div>
          <div class="row">
            <div class="small-8 medium-6 columns">
              <div class="checkbox">
                  <label>
                      {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
                  </label>
              </div>
            </div>
          </div>
          {{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'button expanded', 'style' => 'margin-right:15px']) }}
        {{ Form::close() }}
      </div>
    </div>
</div>

@endsection
