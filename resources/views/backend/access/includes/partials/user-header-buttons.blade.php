<div class="float-right medium-12 hide-for-small-only">
    {{ link_to_route('admin.access.user.index', trans('menus.backend.access.users.all'), [], ['class' => 'button primary tiny']) }}
    {{ link_to_route('admin.access.user.create', trans('menus.backend.access.users.create'), [], ['class' => 'button success tiny']) }}
    {{ link_to_route('admin.access.user.deactivated', trans('menus.backend.access.users.deactivated'), [], ['class' => 'button warning tiny']) }}
    {{ link_to_route('admin.access.user.deleted', trans('menus.backend.access.users.deleted'), [], ['class' => 'button alert tiny']) }}
</div><!--float right-->
{{--  <div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.access.user.index', trans('menus.backend.access.users.all'), [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.access.user.create', trans('menus.backend.access.users.create'), [], ['class' => 'btn btn-success btn-xs']) }}
    {{ link_to_route('admin.access.user.deactivated', trans('menus.backend.access.users.deactivated'), [], ['class' => 'btn btn-warning btn-xs']) }}
    {{ link_to_route('admin.access.user.deleted', trans('menus.backend.access.users.deleted'), [], ['class' => 'btn btn-danger btn-xs']) }}
</div><!--pull right-->  --}}

<div class="float-right medium-10 hide-for-large hide-for-medium">
        <ul class="vertical dropdown menu" data-dropdown-menu>
            <li>{{ trans('menus.backend.access.users.main') }}</li>
            <li>{{ link_to_route('admin.access.user.index', trans('menus.backend.access.users.all')) }}</li>
            <li>{{ link_to_route('admin.access.user.create', trans('menus.backend.access.users.create')) }}</li>
            <li>{{ link_to_route('admin.access.user.deactivated', trans('menus.backend.access.users.deactivated')) }}</li>
            <li>{{ link_to_route('admin.access.user.deleted', trans('menus.backend.access.users.deleted')) }}</li>
        </ul>    
</div><!--float right-->

<div class="clearfix"></div>