@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('strings.backend.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
    {{--  <div class="callout" data-closable>
        <h3 class="box-title">{{ trans('strings.backend.dashboard.welcome') }} {{ $logged_in_user->name }}!</h3>
        <span>
            {!! trans('strings.backend.welcome') !!}
        </span>
    </div><!--callout-->  --}}

    <div class="callout">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
        <span>
            {!! history()->render() !!}
        </span>
    </div><!--box box-success-->
@endsection
